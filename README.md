# BigFrac

`BigFrac` is a rational number library based on `BigInt`

usage:

```js
import { BigFrac } from `bigfrac`

const frac = new BigFrac('4/10') // 2/5

const frac2 = frac.add('3/4') // 23/20
```
