import { decimalToFraction } from './decimalToFraction.js'
import { reduce } from './gcd.js'
import { repeating } from './repeating.js'

declare namespace BigFrac {
  export type Array = [numerator: bigint, denominator: bigint]
  export type Input = bigint | number | string | BigFrac
}

class BigFrac {
  readonly n: bigint
  readonly d: bigint
  static #from(f: BigFrac.Input = 0n): BigFrac.Array {
    if (f instanceof BigFrac) return [f.n, f.d]
    if (typeof f === 'bigint') return [f, 1n]
    return decimalToFraction(f)
  }
  static from(f: BigFrac.Input = 0n): BigFrac {
    return new BigFrac(...BigFrac.#from(f))
  }
  static fromContinuedFraction(cf: BigFrac.Input[]) {
    if (cf.length === 0) return BigFrac.from(0n)
    let frac = BigFrac.from(cf.pop())
    while (cf.length > 0) {
      frac = frac.inv().add(cf.pop())
    }
    return frac
  }
  constructor(n: BigFrac.Input = 0n, d: BigFrac.Input = 1n) {
    const [nn, nd] = BigFrac.#from(n)
    const [dn, dd] = BigFrac.#from(d)
    ;[this.n, this.d] = reduce([nn * dd, nd * dn])
    if (this.d === 0n) throw new RangeError('Division by zero')
  }
  clone() {
    return new BigFrac(this.n, this.d)
  }
  inv() {
    return new BigFrac(this.d, this.n)
  }
  abs() {
    return new BigFrac(this.n < 0n ? -this.n : this.n, this.d)
  }
  sign() {
    if (this.n === 0n) return 0n
    return this.n < 0n ? -1n : 1n
  }
  floor() {
    const n = this.n < 0 ? -this.n : this.n
    const f = this.add(n)
    return f.n / f.d - n
  }
  mul(f: BigFrac.Input = 1n) {
    const [n, d] = BigFrac.#from(f)
    return new BigFrac(this.n * n, this.d * d)
  }
  div(f: BigFrac.Input = 1n) {
    const [n, d] = BigFrac.#from(f)
    return new BigFrac(this.n * d, this.d * n)
  }
  add(f: BigFrac.Input = 0n) {
    const [n, d] = BigFrac.#from(f)
    return new BigFrac(n * this.d + this.n * d, d * this.d)
  }
  sub(f: BigFrac.Input = 0n) {
    return this.add(new BigFrac(f, -1n))
  }
  pow(p = 1n) {
    return p < 0n
      ? new BigFrac(this.d ** -p, this.n ** -p)
      : new BigFrac(this.n ** p, this.d ** p)
  }
  mod(f: BigFrac.Input) {
    f = BigFrac.from(f)
    return f.mul(-this.div(f).floor()).add(this)
  }
  mix(f: BigFrac.Input, a: BigFrac.Input = new BigFrac(1n, 2n)) {
    f = BigFrac.from(f)
    return this.mul(BigFrac.from(1n).sub(a)).add(f.mul(a))
  }
  equal(f: BigFrac.Input) {
    f = BigFrac.from(f)
    return this.d * f.n === this.n * f.d
  }
  valueOf() {
    const n = this.n < 0 ? -this.n : this.n
    const max = n > this.d ? n : this.d
    const div = 2n ** BigInt(max.toString(2).length)
    const mul = 0x20000000000000n
    return Number((this.n * mul) / div) / Number((this.d * mul) / div)
  }
  toString() {
    if (this.d === 1n) return this.n.toString()
    return this.toArray().join('/')
  }
  toRepeating(base = 10) {
    return repeating(this.n, this.d, base)
  }
  toDecimal() {
    return this.toRepeating()
  }
  toArray() {
    return [this.n, this.d] as BigFrac.Array
  }
  toContinuedFraction() {
    let frac: BigFrac = this
    const cf: bigint[] = []
    for (;;) {
      const int = frac.floor()
      const dec = frac.sub(int)
      cf.push(int)
      if (dec.n === 0n) break
      frac = dec.inv()
    }
    return cf
  }
}

export { BigFrac }
