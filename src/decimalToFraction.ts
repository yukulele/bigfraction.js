import type { BigFrac } from './BigFrac.js'
import { reduce } from './gcd.js'

const parseStrRegex =
  /^(\-)?(\d+)(?:(?:\.(?!$))(\d*)?(?:[(](\d+)[)])?)?(?:[eE]([+-]?\d+))?$/
/* -12.34(56)e-7 */

export function parseStr(str: string) {
  const match = str.trim().match(parseStrRegex)
  if (!match) throw new SyntaxError(`ParseError '${str}'`)
  match.shift()
  return match.map((n, i) => {
    if (i === 0) return n || '+'
    if (n != null) return n
    if (i === 1 || i === 4) return '0'
    return ''
  }) as [
    sign: '-' | '+',
    integer: string,
    decimals: string,
    repeatingDecimals: string,
    exponent: string,
  ]
}

function repeatingDecimalToFraction(str: string): BigFrac.Array {
  if (str.includes('/')) {
    const [n, d] = str.split('/', 2).map(repeatingDecimalToFraction)
    return reduce([n[0] * d[1], n[1] * d[0]])
  }
  const [sgn, int, dec, rep, exp] = parseStr(str)

  const sign = sgn === '-' ? -1n : 1n
  const e = BigInt(exp)
  const expNum = e <= 0n ? 1n : 10n ** BigInt(exp)
  const expDen = e >= 0n ? 1n : 10n ** -BigInt(exp)

  const l1 = 10n ** BigInt(dec.length)
  if (rep === '')
    return reduce([sign * BigInt(int + dec) * expNum, l1 * expDen])
  const l2 = l1 * 10n ** BigInt(rep.length)
  const fracD = l2 - l1
  const fracN = BigInt(int + dec + rep) - BigInt(int + dec)
  return reduce([sign * fracN * expNum, fracD * expDen])
}

export function numberToFraction(n: number): [number, number] {
  let rcp = n % 1 === 0 ? 1 : 1 / (n % 1)
  let den = rcp
  const lim = 10
  for (
    let i = 0;
    i < lim &&
    !Number.isInteger(Math.round(rcp * 10 ** (lim - i)) / 10 ** (lim - i));
    i++
  ) {
    rcp = 1 / (rcp % 1)
    den *= rcp
  }
  return [Math.round(n * den), Math.round(den)]
}

export function decimalToFraction(nbr: number | string): BigFrac.Array {
  if (typeof nbr !== 'number') return repeatingDecimalToFraction(nbr)
  return numberToFraction(nbr).map(n => BigInt(n)) as BigFrac.Array
}
