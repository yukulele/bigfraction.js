import type { BigFrac } from './BigFrac.ts'

export function gcd(a: bigint, b: bigint): bigint {
  if (b === 0n) return a
  return gcd(b, a % b)
}

export function reduce([n, d]: BigFrac.Array): BigFrac.Array {
  const div = gcd(n, d)
  n /= div
  d /= div
  if (d < 0n) {
    n *= -1n
    d *= -1n
  }
  return [n, d]
}
