export function repeating(numerator = 0n, denominator = 1n, base = 10) {
  const bigBase = BigInt(base)
  const sign = numerator < 0n ? '-' : ''
  if (sign) numerator = -numerator

  const integerPart = numerator / denominator
  let remainder = numerator % denominator

  if (remainder === 0n) return sign + integerPart.toString(base)

  let fractionalPart = ''
  const remainders = new Map<bigint, number>()

  while (remainder) {
    const cycleStart = remainders.get(remainder)
    if (cycleStart !== undefined) {
      const nonRepeating = fractionalPart.slice(0, cycleStart)
      const repeating = fractionalPart.slice(cycleStart)
      return `${sign}${integerPart.toString(base)}.${nonRepeating}(${repeating})`
    }

    remainders.set(remainder, fractionalPart.length)
    remainder *= bigBase
    fractionalPart += (remainder / denominator).toString(base)
    remainder %= denominator
  }

  return `${sign}${integerPart.toString(base)}.${fractionalPart}`
}
