import { describe, expect, test } from 'bun:test'
import { BigFrac } from '../src/BigFrac.ts'
import { decimalToFraction, parseStr } from '../src/decimalToFraction.ts'

const DecimalToFractionTestCases: [
  input: number | string,
  parsed?: ReturnType<typeof parseStr> | null,
  fraction?: BigFrac.Array,
][] = [
  [1, null, [1n, 1n]],
  [1 / 3, null, [1n, 3n]],
  [0.3333, null, [3333n, 10000n]],
  [''],
  ['1', ['+', '1', '', '', '0'], [1n, 1n]],
  ['-1', ['-', '1', '', '', '0'], [-1n, 1n]],
  ['1e2', ['+', '1', '', '', '2'], [100n, 1n]],
  ['1e-2', ['+', '1', '', '', '-2'], [1n, 100n]],
  ['1.'],
  ['.1'],
  ['1.2', ['+', '1', '2', '', '0'], [6n, 5n]],
  ['1.2e+2', ['+', '1', '2', '', '+2'], [120n, 1n]],
  ['1.2e-2', ['+', '1', '2', '', '-2'], [3n, 250n]],
  ['1.(3)e99', ['+', '1', '', '3', '99'], [4n * 10n ** 99n, 3n]],
  ['1.(2)', ['+', '1', '', '2', '0'], [11n, 9n]],
  ['1.2(3)', ['+', '1', '2', '3', '0'], [37n, 30n]],
  ['1.0(2)', ['+', '1', '0', '2', '0'], [46n, 45n]],
  ['-1.0(2)', ['-', '1', '0', '2', '0'], [-46n, 45n]],
  ['12.34', ['+', '12', '34', '', '0'], [617n, 50n]],
  ['12.34(56)', ['+', '12', '34', '56', '0'], [61111n, 4950n]],
  ['12.34(56)e2', ['+', '12', '34', '56', '2'], [122222n, 99n]],
  ['12.34(56)e-2', ['+', '12', '34', '56', '-2'], [61111n, 495000n]],
  ['12.34(56)2'],
]

describe.each(DecimalToFractionTestCases)(
  'parse input string',
  (input, expectedParsed) => {
    if (typeof input !== 'string') return
    test(input || "''", () => {
      if (expectedParsed == null) expect(() => parseStr(input)).toThrow()
      else expect(parseStr(input)).toEqual(expectedParsed)
    })
  },
)

describe.each(DecimalToFractionTestCases)(
  'toFraction',
  (nbr, _, expectedFraction) => {
    test(String(nbr), () => {
      if (expectedFraction == null)
        expect(() => decimalToFraction(nbr)).toThrow()
      else expect(decimalToFraction(nbr)).toEqual(expectedFraction)
    })
  },
)

// biome-ignore format: keep aligned
const testCasesBigFrac: [
  input: [BigFrac.Input, BigFrac.Input?],
  expected?: BigFrac.Array,
][] = [
  [[3], [3n, 1n]],
  [['4'], [4n, 1n]],
  [[5n], [5n, 1n]],
  [[4n, 6n], [2n, 3n]],
  [[4n, -6n], [-2n, 3n]],
  [[-4n, -6n], [2n, 3n]],
  [[7n, 0]],
  [[4, 6], [2n, 3n]],
  [['4/6'], [2n, 3n]],
  [['4', '6'], [2n, 3n]],
  [['4.2/6.2'], [21n, 31n]],
  [['12.34(56)e-2'], [61111n, 495000n]],
  [['12.34(56)e-2', '54.32(10)e-3'], [611110n, 268889n]],
  [['12.34(56)e-2 /-98.76(54)e-3'], [-611110n, 488889n]],
]
describe.each(testCasesBigFrac)('BibFraction', (input, expected) => {
  test(input.join('/'), () => {
    if (expected == null) expect(() => new BigFrac(...input)).toThrow()
    else expect(new BigFrac(...input).toArray()).toEqual(expected)
  })
})

describe('methods', () => {
  test('add', () => {
    expect(new BigFrac('1').add('2').toDecimal()).toEqual('3')
    expect(new BigFrac('2').add('-7').toDecimal()).toEqual('-5')
    expect(new BigFrac('0.1').add('0.2').toDecimal()).toEqual('0.3')
  })
  test('sub', () => {
    expect(new BigFrac('3').sub('1').toDecimal()).toEqual('2')
    expect(new BigFrac('2.3').sub('3.2').toDecimal()).toEqual('-0.9')
    expect(new BigFrac('-2.5').sub('-3.(1)').toDecimal()).toEqual('0.6(1)')
  })
  test('mul', () => {
    expect(new BigFrac('3/2').mul('5/7').toString()).toEqual('15/14')
    expect(new BigFrac('3/2').mul('-5/7').toString()).toEqual('-15/14')
    expect(new BigFrac('3/2').mul('0').toString()).toEqual('0')
  })
  test('div', () => {
    expect(new BigFrac('3/2').div('7/5').toString()).toEqual('15/14')
    expect(new BigFrac('3/2').div('-7/5').toString()).toEqual('-15/14')
    expect(() => new BigFrac('3/2').div('0')).toThrow('Division by zero')
  })
  test('pow', () => {
    expect(new BigFrac('3/2').pow(2n).toString()).toEqual('9/4')
    expect(new BigFrac('3/2').pow(-3n).toString()).toEqual('8/27')
  })
  test('mod', () => {
    expect(new BigFrac('10').mod('4').toString()).toEqual('2')
    expect(new BigFrac('10').mod('-4').toString()).toEqual('-2')
    expect(new BigFrac('10/3').mod('4/5').toString()).toEqual('2/15')
  })
  test('mix', () => {
    expect(new BigFrac('2').mix('6').toString()).toEqual('4')
    expect(new BigFrac('2').mix('6', '0.75').toString()).toEqual('5')
  })
  test('clone, equal', () => {
    const frac1 = new BigFrac('3.25(7)')
    const frac2 = frac1.clone()
    expect(frac2.toDecimal()).toEqual('3.25(7)')
    expect(frac1 === frac2).toBeFalse()
    expect(frac1.equal(frac2)).toBeTrue()
    expect(frac2.equal(frac1)).toBeTrue()
  })
  test('inv', () => {
    expect(new BigFrac('2.5').inv().toDecimal()).toEqual('0.4')
  })
  test('abs', () => {
    expect(new BigFrac('12.4').abs().toDecimal()).toEqual('12.4')
    expect(new BigFrac('-2.3').abs().toDecimal()).toEqual('2.3')
  })
  test('sign', () => {
    expect(new BigFrac('5').sign()).toEqual(1n)
    expect(new BigFrac('-5').sign()).toEqual(-1n)
  })
  test('floor', () => {
    expect(new BigFrac('3').floor()).toEqual(3n)
    expect(new BigFrac('3.23').floor()).toEqual(3n)
    expect(new BigFrac('3.7').floor()).toEqual(3n)
    expect(new BigFrac('-3').floor()).toEqual(-3n)
    expect(new BigFrac('-3.23').floor()).toEqual(-4n)
    expect(new BigFrac('-3.7').floor()).toEqual(-4n)
  })
  test('valueOf', () => {
    expect(new BigFrac('1/3').valueOf()).toEqual(1 / 3)
    expect(new BigFrac('3/7').valueOf()).toEqual(3 / 7)
    expect(new BigFrac('3777/7341').valueOf()).toEqual(3777 / 7341)
    const a = '342156789453'.repeat(50) + 8
    const b = '245672144687'.repeat(50) + 9
    expect(new BigFrac(a, b).valueOf()).toEqual(1.3927374220179776)
  })
  test('toString', () => {
    expect(new BigFrac('1.5').toString()).toEqual('3/2')
    expect(new BigFrac('7').toString()).toEqual('7')
  })
  test('toDecimal', () => {
    expect(new BigFrac('0').toDecimal()).toEqual('0')
    expect(new BigFrac('-1/2').toDecimal()).toEqual('-0.5')
    expect(new BigFrac('2/30').toDecimal()).toEqual('0.0(6)')
    expect(new BigFrac('4000/3').toDecimal()).toEqual('1333.(3)')
  })
  test('toRepeating', () => {
    expect(new BigFrac('13/7').toRepeating()).toEqual('1.(857142)')
    expect(new BigFrac('13/7').toRepeating(2)).toEqual('1.(110)')
    expect(new BigFrac('13/7').toRepeating(3)).toEqual('1.(212010)')
    expect(new BigFrac('13/7').toRepeating(6)).toEqual('1.(50)')
    expect(new BigFrac('13/7').toRepeating(7)).toEqual('1.6')
    expect(new BigFrac('13/7').toRepeating(16)).toEqual('1.(db6)')
    expect(new BigFrac('13/7').toRepeating(36)).toEqual('1.(u)')
  })
  test('toArray', () => {
    expect(new BigFrac('0.2').toArray()).toEqual([1n, 5n])
  })
  test('toContinuedFraction', () => {
    expect(new BigFrac('0').toContinuedFraction()).toEqual([0n])
    expect(new BigFrac('1.5').toContinuedFraction()).toEqual([1n, 2n])
    expect(new BigFrac('-1.5').toContinuedFraction()).toEqual([-2n, 2n])
    expect(new BigFrac('1.(3)').toContinuedFraction()).toEqual([1n, 3n])
    expect(new BigFrac('-1.(6)').toContinuedFraction()).toEqual([-2n, 3n])
    expect(new BigFrac('-2.5(4)').toContinuedFraction()).toEqual([
      -3n,
      2n,
      5n,
      8n,
    ])
  })
})

describe('static', () => {
  test.each([
    [[0n], '0'],
    [[1n, 2n], '1.5'],
    [[-2n, 2n], '-1.5'],
    [[1n, 3n], '1.(3)'],
    [[-2n, 3n], '-1.(6)'],
  ])('fromContinuedFraction %p = %s', (cf, dec) => {
    expect(BigFrac.fromContinuedFraction(cf).toDecimal()).toBe(dec)
  })
})
